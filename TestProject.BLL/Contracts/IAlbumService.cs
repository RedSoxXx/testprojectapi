﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestProject.BLL.Models;

namespace TestProject.BLL.Contracts
{
    public interface IAlbumService
    {
        Task<IEnumerable<Album>> GetAllAlbumsAsync();

        Task<Album> GetAlbumByIdAsync(int albumId);

        Task<IEnumerable<Album>> GetAlbumByUserIdAsync(int userId);
    }
}
