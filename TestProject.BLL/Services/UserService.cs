﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestProject.BLL.Contracts;
using TestProject.BLL.Models;
using TestProject.BLL.Services.Base;

namespace TestProject.BLL.Services
{
    public class UserService : BaseApiService, IUserService
    {
        private readonly string _getAllUsersUrl = "http://jsonplaceholder.typicode.com/users";
        private readonly string _getUserUrl = "http://jsonplaceholder.typicode.com/users/{0}";

        public async Task<IEnumerable<User>> GetAllUsersAsync()
        {
            return await GetAsync<User[]>(_getAllUsersUrl);
        }

        public async Task<User> GetUserByIdAsync(int userId)
        {
            return await GetAsync<User>(string.Format(_getUserUrl, userId));
        }
    }
}
