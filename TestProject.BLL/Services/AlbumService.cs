﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestProject.BLL.Contracts;
using TestProject.BLL.Models;
using TestProject.BLL.Services.Base;

namespace TestProject.BLL.Services
{
    public class AlbumService : BaseApiService, IAlbumService
    {
        private readonly string _getAllAlbumsUrl = "http://jsonplaceholder.typicode.com/albums";
        private readonly string _getAlbumUrl = "http://jsonplaceholder.typicode.com/albums/{0}";
        private readonly string _getUserAlbumsUrl = "http://jsonplaceholder.typicode.com/users/{0}/albums";


        public async Task<IEnumerable<Album>> GetAllAlbumsAsync()
        {
            return await GetAsync<Album[]>(_getAllAlbumsUrl);
        }

        public async Task<Album> GetAlbumByIdAsync(int albumId)
        {
            return await GetAsync<Album>(string.Format(_getAlbumUrl, albumId));
        }

        public async Task<IEnumerable<Album>> GetAlbumByUserIdAsync(int userId)
        {
            return await GetAsync<Album[]>(string.Format(_getUserAlbumsUrl, userId));
        }
    }
}
