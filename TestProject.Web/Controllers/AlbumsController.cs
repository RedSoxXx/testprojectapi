﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using TestProject.BLL.Contracts;
using TestProject.BLL.Models;

namespace TestProject.Web.Controllers
{
    public class AlbumsController : ApiController
    {
        private readonly IAlbumService _albumService;

        public AlbumsController(IAlbumService albumService)
        {
            _albumService = albumService;
        }
        // GET api/albums
        public async Task<IEnumerable<Album>> Get()
        {
            return await _albumService.GetAllAlbumsAsync();
        }

        // GET api/albums/5
        public async Task<Album> Get(int id)
        {
            return await _albumService.GetAlbumByIdAsync(id);
            
        }
    }
}
