﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using TestProject.BLL.Contracts;
using TestProject.BLL.Models;

namespace TestProject.Web.Controllers
{
    public class UsersController : ApiController
    {
        private readonly IUserService _userService;
        private readonly IAlbumService _albumService;

        public UsersController(IUserService userService, IAlbumService albumService)
        {
            _userService = userService;
            _albumService = albumService;
        }
        // GET api/users
        public async Task<IEnumerable<User>> Get()
        {
            var users = await _userService.GetAllUsersAsync();
            return Mapper.Map<User[]>(users, opt => opt.Items["IsAuthenticated"] = User.Identity.IsAuthenticated);
        }

        // GET api/users/5
        public async Task<User> Get(int id)
        {
            var user = await _userService.GetUserByIdAsync(id);
            return Mapper.Map<User>(user, opt => opt.Items["IsAuthenticated"] = User.Identity.IsAuthenticated);
        }

        [Route("api/users/{userId}/albums")]
        public async Task<IEnumerable<Album>> GetAlbumsByUser(int userId)
        {
            return await _albumService.GetAlbumByUserIdAsync(userId);
        }
    }
}
