﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using AutoMapper;
using TestProject.BLL.Models;

namespace TestProject.Web.Mappings
{
    public class DataMappingsProfileWeb : Profile
    {
        public DataMappingsProfileWeb()
        {
            CreateMap<User, User>()

                .ConvertUsing((viewModel, dataModel, context) =>
                {
                    var model = viewModel;
                    if (!Convert.ToBoolean(context.Items["IsAuthenticated"]))
                    {
                        var md5 = MD5.Create();
                        var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(model.Email));
                        model.Email = Convert.ToBase64String(hash);
                    }
                    return model;
                });
        }

    }
}