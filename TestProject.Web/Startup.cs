﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using TestProject.Web.App_Start;

[assembly: OwinStartup(typeof(TestProject.Web.Startup))]

namespace TestProject.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AutofacConfig.RegisterComponents(app);
            ConfigureAuth(app);
        }
    }
}
